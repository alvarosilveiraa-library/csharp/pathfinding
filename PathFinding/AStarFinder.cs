using System;
using System.Collections.Generic;

namespace PathFinding
{
    public class AStarFinder
    {
        public static List<Point> FindPath(Grid grid, Point startPoint, Point targetPoint)
        {
            List<Node> nodes = ImpFindPath(grid, startPoint, targetPoint);

            List<Point> ret = new List<Point>();

            if (nodes != null)
            {
                foreach (Node node in nodes)
                {
                    ret.Add(new Point(node.x, node.y));
                }
            }

            return ret;
        }

        private static List<Node> ImpFindPath(Grid grid, Point startPoint, Point targetPoint)
        {
            Node startNode = grid.nodes[startPoint.x, startPoint.y];

            Node targetNode = grid.nodes[targetPoint.x, targetPoint.y];

            List<Node> openSet = new List<Node>();

            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                Node currentNode = openSet[0];

                for (int i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].f < currentNode.f || openSet[i].f == currentNode.f && openSet[i].h < currentNode.h)
                    {
                        currentNode = openSet[i];
                    }
                }

                openSet.Remove(currentNode);

                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    return RetracePath(grid, startNode, targetNode);
                }

                foreach (Node neighbour in grid.GetNeighbours(currentNode))
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour)) continue;

                    int newMovementCostToNeighbour = currentNode.g + GetDistance(currentNode, neighbour) * (int)(10.0f * neighbour.penalty);

                    if (newMovementCostToNeighbour < neighbour.g || !openSet.Contains(neighbour))
                    {
                        neighbour.g = newMovementCostToNeighbour;

                        neighbour.h = GetDistance(neighbour, targetNode);

                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour)) openSet.Add(neighbour);
                    }
                }
            }

            return null;
        }

        private static List<Node> RetracePath(Grid grid, Node startNode, Node endNode)
        {
            List<Node> path = new List<Node>();

            Node currentNode = endNode;

            while (currentNode != startNode)
            {
                path.Add(currentNode);

                currentNode = currentNode.parent;
            }

            path.Reverse();

            return path;
        }

        private static int GetDistance(Node nodeA, Node nodeB)
        {
            int dstX = Math.Abs(nodeA.x - nodeB.x);

            int dstY = Math.Abs(nodeA.y - nodeB.y);

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }
    }

}
