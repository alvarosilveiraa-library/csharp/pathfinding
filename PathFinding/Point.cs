namespace PathFinding
{
    public class Point
    {
        public int x;
        public int y;

        public Point()
        {
            this.x = 0;
            this.y = 0;
        }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Point(Point point)
        {
            this.x = point.x;
            this.y = point.y;
        }

        public override int GetHashCode()
        {
            return x ^ y;
        }

        public override bool Equals(System.Object obj)
        {
            Point point = (Point)obj;

            if (ReferenceEquals(null, point))
            {
                return false;
            }

            return (this.x == point.x) && (this.y == point.y);
        }

        public bool Equals(Point point)
        {
            if (ReferenceEquals(null, point))
            {
                return false;
            }

            return (this.x == point.x) && (this.y == point.y);
        }

        public static bool operator ==(Point pointA, Point pointB)
        {
            if (System.Object.ReferenceEquals(pointA, pointB))
            {
                return true;
            }
            if (ReferenceEquals(null, pointA))
            {
                return false;
            }
            if (ReferenceEquals(null, pointB))
            {
                return false;
            }

            return pointA.x == pointB.x && pointA.y == pointB.y;
        }

        public static bool operator !=(Point pointA, Point pointB)
        {
            return !(pointA == pointB);
        }

        public Point Set(int x, int y)
        {
            this.x = x;

            this.y = y;

            return this;
        }
    }
}
