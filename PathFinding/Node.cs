namespace PathFinding
{
    public class Node
    {
        public bool walkable;

        public int x;

        public int y;

        public float penalty;

        public int g;

        public int h;

        public Node parent;

        public Node(float price, int x, int y)
        {
            this.walkable = price != 0.0f;

            this.penalty = price;

            this.x = x;

            this.y = y;
        }

        public int f
        {
            get
            {
                return g + h;
            }
        }
    }
}
