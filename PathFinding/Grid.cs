using System.Collections.Generic;

namespace PathFinding
{
    public class Grid
    {
        public Node[,] nodes;

        private int width;

        private int height;

        public Grid(int width, int height, float[,] tiles_costs)
        {
            this.width = width;
            this.height = height;
            this.nodes = new Node[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    this.nodes[x, y] = new Node(tiles_costs[x, y], x, y);
                }
            }
        }

        public Grid(int width, int height, bool[,] walkable_tiles)
        {
            this.width = width;
            this.height = height;
            this.nodes = new Node[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    this.nodes[x, y] = new Node(walkable_tiles[x, y] ? 1.0f : 0.0f, x, y);
                }
            }
        }

        public List<Node> GetNeighbours(Node node)
        {
            List<Node> neighbours = new List<Node>();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0) continue;

                    int checkX = node.x + x;
                    int checkY = node.y + y;

                    if (checkX >= 0 && checkX < width && checkY >= 0 && checkY < height)
                    {
                        neighbours.Add(this.nodes[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }
    }
}
